rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[0.0, 3.5, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[0.0, 5.0, 0.0]' '[0.0, 0.0, -4.5]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[0.0, 1.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[-2.3, -1.0, 0.0]' '[0.0, 0.0, 0.0]'

rosservice call /spawn 9.5 8.5 0 ""
rosservice call /turtle2/set_pen 255 100 100 6 0
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
	-- '[0.0, -0.3, 0.0]' '[0.0, 0.0, 0.0]'
rosservice call /turtle2/set_pen 0 0 0 0 1
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
	-- '[0.0, -0.3, 0.0]' '[0.0, 0.0, 0.0]'
rosservice call /turtle2/set_pen 255 100 100 2 0
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
	-- '[0.0, -2.2, 0.0]' '[0.0, 0.0, 0.0]'
